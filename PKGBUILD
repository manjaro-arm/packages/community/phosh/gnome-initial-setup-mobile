# Maintainer: Philip Müller <philm at manjaro dot org>
# Contributor: Balló György <ballogyor+arch at gmail dot com>
# Contributor: Fabian Bornschein <fabiscafe@archlinux.org>

pkgname=gnome-initial-setup-mobile
_pkgname=gnome-initial-setup
pkgver=46.0
pkgrel=1
pkgdesc="Helps you to set up your OS when you boot for the first time - Modified for Mobile"
arch=('x86_64' 'armv7h' 'aarch64')
url="https://gitlab.gnome.org/GNOME/gnome-initial-setup"
license=('GPL-2.0-or-later')
depends=(
    'accountsservice'
    'fontconfig'
    'gcc-libs'
    'gdk-pixbuf2'
    'gdm'
    'geoclue'
    'geocode-glib-2'
    'glib2'
    'glibc'
    'gnome-desktop-4'
    'gnome-control-center'
    'gnome-keyring'
    'gsettings-desktop-schemas'
    'gtk4'
    'harfbuzz'
    'krb5'
    'libadwaita'
    'libgdm'
    'libgweather-4'
    'libibus'
    'libmalcontent'
    'libnm'
    'libnma-gtk4'
    'libpwquality'
    'libsecret'
    'malcontent'
    'pango'
    'polkit'
    'tecla'
    'webkitgtk-6.0'
)
makedepends=(
    'git'
    'glib2-devel'
    'meson'
)
source=(
  "git+https://gitlab.gnome.org/GNOME/gnome-initial-setup.git?signed#tag=${pkgver/[a-z]/.&}"
  'Revert-Disable-existing-user-mode.patch'
)
sha256sums=('969d4fce6e790ac1344a157ab07ce5ffa0964663f837649315302c19dc871f5e'
            '99b37e4ce333f86707ff77bf1e0cc369ec743a1d9993313ee181d22ded2fe847')
validpgpkeys=(
  1E68E58CF255888301645B563422DC0D7AD482A7 # Will Thompson <will@willthompson.co.uk>
)

prepare() {
  cd "$_pkgname"
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch $src..."
    patch -Np1 < "../$src"
  done
}

build() {
  arch-meson "$_pkgname" build -Dparental_controls=disabled
  meson compile -C build
}

package() {
  meson install -C build --destdir "$pkgdir"

  # Setup system user and group
  echo 'u gnome-initial-setup - "GNOME Initial Setup" /run/gnome-initial-setup' |
    install -Dm644 /dev/stdin "$pkgdir/usr/lib/sysusers.d/$_pkgname.conf"
  echo 'd /run/gnome-initial-setup 0700 gnome-initial-setup gnome-initial-setup -' |
    install -Dm644 /dev/stdin "$pkgdir/usr/lib/tmpfiles.d/$_pkgname.conf"
}
